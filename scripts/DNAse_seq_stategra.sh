#!/bin/bash
#SBATCH --time=0:10:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=3M
#SBATCH --cpus-per-task=1
#SBATCH --job-name=dnase_seq_stategra
#SBATCH --partition=normal
#SBATCH -o /home/users/student01/cluster_de_calcul/scripts/DNAse_seq_stategra.log

echo "Lilian Faurie - M2BI"
echo "Date: 30/11/2023"
echo -e "Object: Example workflow for DNAse Seq Stategra datasets showing task execution and dependency management.\n"
echo "Inputs: Paths to scripts for quality control, sorting, mapping, peak calling, multiple coverage and R statistics."
echo "Outputs: Fastq files, trimmed fastq files, QC HTML files, SAM and sort BAM mapping files, BAI index files, BED files, TSV matrix file."
echo -e "Logs: Scripts logs are generated in log folder. \n"


# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose



#                                  DNASEse-seq TREATMENT PIPELINE


#ROOT-------------------------------------------------------------------------------------------------

jid0=$(sbatch --parsable 1_root.slurm)
echo "$jid0 : (Script: 1_root.slurm) - Root"

#TOOLS------------------------------------------------------------------------------------------------

jid1=$(sbatch --parsable --dependency=afterok:$jid0 0_pipeline_tools.slurm)
echo "$jid1 : (Script: 0_pipeline_tools.slurm) - Tools"

#RAW DATA QUALITY-------------------------------------------------------------------------------------

#QC
jid2=$(sbatch --parsable --dependency=afterok:$jid1 2_qc_raw_data.slurm)
echo "$jid2 : (Script: 2_qc_raw_data.slurm) - QC raw data"

#MultiQC
jid3=$(sbatch --parsable --dependency=afterok:$jid2 3_multiqc_raw_data.slurm)
echo "$jid3 : (Script: 3_multiqc_raw_data.slurm) - MultiQC raw data"

#TRIM-------------------------------------------------------------------------------------------------

jid4=$(sbatch --parsable --dependency=afterok:$jid1 4_trim_raw_data.slurm)
echo "$jid4 : (Script: 4_trim_raw_data.slurm) - Trimming raw data"

#TRIM DATA QUALITY------------------------------------------------------------------------------------

#QC
jid5=$(sbatch --parsable --dependency=afterok:$jid4 5_qc_trim_data.slurm)
echo "$jid5 : (Script: 5_qc_trim_data.slurm) - QC trim data"

#MultiQC
jid6=$(sbatch --parsable --dependency=afterok:$jid5 6_multiqc_trim_data.slurm)
echo "$jid6 : (Script: 6_multiqc_trim_data.slurm) - MultiQC trim data"

#MAP--------------------------------------------------------------------------------------------------

jid7=$(sbatch --parsable --dependency=afterok:$jid4 7_map_trim_data.slurm)
echo "$jid7 : (Script: 7_map_trim_data.slurm) - Map trim data"

#PEAK CALLING-----------------------------------------------------------------------------------------

jid8=$(sbatch --parsable --dependency=afterok:$jid1:$jid4:$jid7 8_peak_trim_data.slurm)
echo "$jid8 : (Script: 8_peak_trim_data.slurm) - Peak calling trim data"

#FILES TREATMENTS-------------------------------------------------------------------------------------

#Merge peaks
jid9=$(sbatch --parsable --dependency=afterok:$jid8 9_merge_peak_data.slurm)
echo "$jid9 : (Script: 9_merge_peak_data.slurm) - Merge peaks files"

#BAM conversion
jid10=$(sbatch --parsable --dependency=afterok:$jid7 10_sam_to_bam_data.slurm)
echo "$jid10 : (Script: 10_sam_to_bam_data.slurm) - Convert SAM to BAM files"

#Index BAM
jid11=$(sbatch --parsable --dependency=afterok:$jid10 11_index_bam_data.slurm)
echo "$jid11 : (Script: 11_index_bam_data.slurm) - Indexation of BAM files"

#MATRIX-----------------------------------------------------------------------------------------------

jid12=$(sbatch --parsable --dependency=afterok:$jid11:$jid9 12_count_matrix_data.slurm)
echo "$jid12 : (Script: 12_count_matrix_data.slurm) - Count Matrix"

#R ANALYSIS-------------------------------------------------------------------------------------------

jid13=$(sbatch --parsable --dependency=afterok:$jid12 13_r_analysis_data.slurm)
echo "$jid13 : (Script: 13_r_analysis_data.slurm) - R treatments"

#PIPELINE INFORMATION---------------------------------------------------------------------------------

jid14=$(sbatch --parsable --dependency=afterok:$jid13 14_pipeline_info_data.slurm)
echo "$jid14 : (Script: 14_pipeline_info_data.slurm) - Execution statistics"

#-----------------------------------------------------------------------------------------------------


#                                            END





# Display information in logs
echo -e "\nDNAse-seq analysis completed\n\nGeneral task information\n"



