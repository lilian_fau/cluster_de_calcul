#!/bin/bash
#SBATCH --time=0:10:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=1500M
#SBATCH --cpus-per-task=8
#SBATCH --job-name=trim_raw_data
#SBATCH --partition=normal
#SBATCH --array=0-27
#SBATCH -o /home/users/student01/cluster_de_calcul/log/job_%A_%a_trim_raw_data.out



# This fourth step cleans the raw DNAse-seq data using the trimmomatic tool to remove adapters and poly-G tails.



# Load modules
ml java trimmomatic/0.38

# Working directory
DATADIR="/home/users/shared/data/stategra/dna-seq"
WKDIR="/home/users/student01/cluster_de_calcul"
TRIMDIR="$WKDIR/data/2.trimming"
TRIMMOMATIC_JAR="/opt/apps/trimmomatic-0.38/trimmomatic-0.38.jar"
ADAPTER_FILE="$TRIMDIR/TruSeq3-PE.fa"
FASTQ_FILES=($(find "$DATADIR" -type f -name "*.fastq.gz"))

# Download the NexteraPE-PE.fa adapter file from GitHub
if [ ! -f "$ADAPTER_FILE" ]; then
    wget https://raw.githubusercontent.com/usadellab/Trimmomatic/main/adapters/TruSeq3-PE-2.fa -O "$ADAPTER_FILE"
fi

# Retrieve the FASTQ file corresponding to the array task index
BASENAME=$(basename "${FASTQ_FILES[$SLURM_ARRAY_TASK_ID]}")
SRA=$(echo $BASENAME | sed 's/_[12].fastq.gz//')

# Perform trimming
java -jar $TRIMMOMATIC_JAR PE \
    -threads $SLURM_CPUS_PER_TASK \
    "$DATADIR/${SRA}_1.fastq.gz" \
    "$DATADIR/${SRA}_2.fastq.gz" \
    "$TRIMDIR/trimmed_${SRA}_1_P.fastq.gz" \
    "$TRIMDIR/trimmed_${SRA}_1_U.fastq.gz" \
    "$TRIMDIR/trimmed_${SRA}_2_P.fastq.gz" \
    "$TRIMDIR/trimmed_${SRA}_2_U.fastq.gz" \
    ILLUMINACLIP:"$ADAPTER_FILE":2:30:10 \
    LEADING:5 TRAILING:5 SLIDINGWINDOW:4:5 MINLEN:25

# Remove unpaired files
rm $TRIMDIR/trimmed_${SRA}_1_U.fastq.gz $TRIMDIR/trimmed_${SRA}_2_U.fastq.gz

# Move logs 
mkdir -p ../log/"$SLURM_JOB_NAME"_log 
echo -e "\n\n"
find ../log/ -type f -name "*$SLURM_JOB_NAME*" -exec mv {} ../log/"$SLURM_JOB_NAME"_log \;

# Display information in logs
echo -e "\nTrimming completed"
echo -e "\nTask information:\n"
sacct -j $SLURM_JOB_ID --units M --format="JobID,JobName,User,AllocCPUS,CPUTime,Elapsed,state%20"