#!/bin/bash
#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=500M
#SBATCH --cpus-per-task=12
#SBATCH --job-name=r_analysis_data
#SBATCH --partition=normal
#SBATCH -o /home/users/student01/cluster_de_calcul/log/job_%A_r_analysis_data.out



# This third script performs the various statistical processes on the counting matrix using R and the NOIseq package. In this script we manually install the R package "Noiseq" from a temporary library because we don't have the write permissions to install them directly on the server. To do this, we redirect the installation and execution of the library using the command: R_LIBS_USER="/home/users/student01/cluster_de_calcul/tmp/".



# Load modules
ml gcc/8.1.0 R/4.0.2

# NO WKDIR HERE ==> R DO NOT DETECT VARIABLES ==> MANUAL MODIFICATIONS

# Create a temporary directory
mkdir -p ../tmp

# Install BiocManager package to manage Bioconductor packages
# Use BiocManager to install the NOISeq package
R_LIBS_USER="/home/users/student01/cluster_de_calcul/tmp/" Rscript -e 'install.packages("BiocManager", lib="/home/users/student01/cluster_de_calcul/tmp/", repos="https://cran.rstudio.com/"); BiocManager::install("NOISeq", lib="/home/users/student01/cluster_de_calcul/tmp/", dependencies = TRUE, force = TRUE)'

# Run R script for analysis
R_LIBS_USER="/home/users/student01/cluster_de_calcul/tmp/" Rscript -e 'source("/home/users/student01/cluster_de_calcul/scripts/analysis.R")'

# Remove the temporary directory
rm -r ../tmp

# Copy the statistically processed count matrix file to the results folder.
cp /home/users/student01/cluster_de_calcul/data/6.r_analysis/STAT_DNaseSeq_homer_RPKM_TMM_ARSyN.txt /home/users/student01/cluster_de_calcul/results

# Move logs 
mkdir -p ../log/"$SLURM_JOB_NAME"_log 
echo -e "\n\n"
find ../log/ -type f -name "*$SLURM_JOB_NAME*" -exec mv {} ../log/"$SLURM_JOB_NAME"_log \;

echo -e "\nTask information:\n"
sacct -j $SLURM_JOB_ID --units M --format="JobID,JobName,User,AllocCPUS,CPUTime,Elapsed,state%20"
